<!-- the code below is used to link the layout -->
@extends('layouts.app')

<!-- the code below is used to link the content -->
@section('content')

    <!-- the code below is used to bring the header -->
<ul class="collection with-header">
    <li class="collection-header">
        <!-- the code below is used to bring the ethics section  -->
        <a href="https://docs.google.com/document/d/11A2tdoIquIEk-_hHw0TdA0BRCvscXMqPcftLluJc6sQ/edit?usp=sharing">Ethics for all surveys</a>
        <!-- the code below is used to show the recent surveys -->
        <h2 class="flow-text">Recent Surveys
            <!-- the code below is used to link the routs -->
            <span style="float:right;">{{ link_to_route('new.survey', 'Create new') }}
            </span>
        </h2>
    </li>
    <!-- the code below is used to bring the options to send and also the option to edit -->
    @forelse ($surveys as $survey)
        <li class="collection-item">
            <div>
                {{ link_to_route('detail.survey', $survey->title, ['id'=>$survey->id])}}
                <a href="survey/view/{{ $survey->id }}" title="Take Survey" class="secondary-content"><i class="material-icons">send</i></a>
                <a href="survey/{{ $survey->id }}" title="Edit Survey" class="secondary-content"><i class="material-icons">edit</i></a>
            </div>
        </li>
    @empty
    <!-- the code below is used if there is nothing to show in the list  -->
        <p class="flow-text center-align">Nothing to show</p>
    @endforelse
</ul>
@endsection
