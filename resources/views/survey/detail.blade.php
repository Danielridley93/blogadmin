<!-- the code below is used to set the layout -->
@extends('layout')
<!-- the code below is used to set the content  -->
@section('content')
    <!-- the code below is used to set the card content title -->
  <div class="card">
      <div class="card-content">
      <span class="card-title"> {{ $survey->title }}</span>
      <p>
          <!-- the code below is used to set the survey discription-->
        {{ $survey->description }}
      </p>
      <br/>
          <!-- the code below is used to set the take survey the edit and ethics-->
      <a href='view/{{$survey->id}}'>Take Survey</a> | <a href="{{$survey->id}}/edit">Edit Survey</a> | <a href="https://docs.google.com/document/d/11A2tdoIquIEk-_hHw0TdA0BRCvscXMqPcftLluJc6sQ/edit?usp=sharing">Ethics for all surveys</a> <a href="#doDelete" style="float:right;" class="modal-trigger red-text">Delete Survey</a>
          <!-- the code below is used to set the delete -->
      <div id="doDelete" class="modal bottom-sheet">
          <!-- the code below is used to set the modle content-->
        <div class="modal-content">
            <!-- the code below is used to set the container-->
          <div class="container">
              <!-- the code below is used to set the row-->
            <div class="row">
                <!-- the code below is used to see if the user wants to delete the survey-->
              <h4>Are you sure?</h4>
              <p>Do you wish to delete this survey called "{{ $survey->title }}"?</p>
                <!-- the code below is used to set the modle footer-->
              <div class="modal-footer">
                  <!-- the code below is used to ask if the user wants to delete or cancel-->
                <a href="/survey/{{ $survey->id }}/delete" class=" modal-action waves-effect waves-light btn-flat red-text">Yes</a>
                <a class=" modal-action modal-close waves-effect waves-light green white-text btn">Cancel</a>
              </div>
            </div>
          </div>
        </div>
      </div>
          <!-- the code below is used to set the style divider-->
      <div class="divider" style="margin:20px 0px;"></div>
          <!-- the code below is used to set the questions title-->
      <p class="flow-text center-align">Questions</p>
          <!-- the code below is used to set the expandable list -->
      <ul class="collapsible" data-collapsible="expandable">
          <!-- the code below is used to set the questions -->
          @forelse ($survey->questions as $question)
              <!-- the code below is used to set the style-->
          <li style="box-shadow:none;">
              <!-- the code below is used to set the title and id and the question edit option  -->
            <div class="collapsible-header">{{ $question->title }} <a href="/question/{{ $question->id }}/edit" style="float:right;">Edit</a></div>
              <!-- the code below is used to collapse the body-->
            <div class="collapsible-body">
                <!-- the code below is used to set the style-->
              <div style="margin:5px; padding:10px;">
                  <!-- the code below is used to open the form  -->
                  <!-- the code below is used to open the form  -->
              {!! Form::open() !!}
              <!-- the code below is used to set the question type as text-->
                  @if($question->question_type === 'text')
                      {{ Form::text('title')}}
                      <div class="row">
                          <div class="input-field col s12">
                              <textarea id="textarea1" class="materialize-textarea"></textarea>
                              <label for="textarea1">Provide answer</label>
                          </div>
                      </div>
                      <!-- the code below is the attempt at doing a check box but did not work as expected so it failed-->
                  @elseif($question->question_type === 'checkbox')
                      @foreach($question->option_name as $key=>$value)
                          <p style="margin:0px; padding:0px;">
                              <input type="checkbox" id="{{ $key }}" />
                              <label for="{{$key}}">{{ $value }}</label>
                          </p>
                      @endforeach
                  @endif
              <!-- the code below is used to close the form-->
                  {!! Form::close() !!}
              </div>
            </div>
          </li>
          @empty
              <!-- the code below is used to show there are no questions-->
            <span style="padding:10px;">Nothing to show. Add questions below.</span>
          @endforelse
      </ul>
          <!-- the code below is used to add questions lable-->
      <h2 class="flow-text">Add Question</h2>
          <!-- the code below is used to set a survey id-->
      <form method="POST" action="{{ $survey->id }}/questions" id="boolean">
          <!-- the code below is used to set the token csrf -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <!-- the code below is used to set the row-->
        <div class="row">
          <div class="input-field col s12">
              <!-- the code below is used to set the browser default -->
            <select class="browser-default" name="question_type" id="question_type">
                <!-- the code below is used to set the choose option-->
              <option value="" disabled selected>Choose your option</option>
                <!-- the code below is used to set the text option-->
              <option value="text">Text</option>
              <option value="checkbox">Checkbox</option>


            </select>
          </div>
            <!-- the code below is used to set the title and text -->
          <div class="input-field col s12">
            <input name="title" id="title" type="text">
              <!-- the code below is a lable-->
            <label for="title">Question</label>
          </div>
            <!-- the code below is used to set the submit of the form-->
          <span class="form-g"></span>

          <div class="input-field col s12">
          <button class="btn waves-effect waves-light">Submit</button>
          </div>
        </div>
        </form>
    </div>
  </div>
@stop