<!-- the code below is used to bring the layout  -->
@extends('layout')
<!-- the code below is used to bring the content   -->
@section('content')
    <!-- the code below is used to create the card content  -->
  <div class="card">
      <div class="card-content">
          <!-- the code below is used to be a lable to start taking the survey  -->
      <span class="card-title"> Start taking Survey</span>
      <p>
          <!-- the code below is used to create the title  -->
        <span class="flow-text">{{ $survey->title }}</span> <br/>
      </p>
      <p>
          <!-- the code below is used to display the description  -->
        {{ $survey->description }}
      <!-- the code below is used to show the username   -->
        <br/>Created by: <a href="">{{ $survey->user->name }}</a>
      </p>
          <!-- the code below is used to set the style divider  -->
      <div class="divider" style="margin:20px 0px;"></div>
          <!-- the code below is used to open the ansewrs controller  -->
          {!! Form::open(array('action'=>array('AnswerController@store', $survey->id))) !!}
      <!-- the code below is used to loop the questions  -->
          @forelse ($survey->questions as $key=>$question)
              <!-- the code below is used to set the questions title  -->
            <p class="flow-text">Question {{ $key+1 }} - {{ $question->title }}</p>
                  <!-- the code below is used to set the question type   -->
                @if($question->question_type === 'text')
                    <!-- the code below is used to ansewrs to the questions   -->
                  <div class="input-field col s12">
                    <input id="answer" type="text" name="{{ $question->id }}[answer]">
                      <!-- the code below is a lable  -->
                    <label for="answer">Answer</label>
                  </div>
                        <!-- the code below is used to set the questions   -->
                @elseif($question->question_type === 'textarea')
                    <!-- the code below is used to set the ansers in place  -->
                  <div class="input-field col s12">
                    <textarea id="textarea1" class="materialize-textarea" name="{{ $question->id }}[answer]"></textarea>
                      <!-- the code below is a lable -->
                    <label for="textarea1">Textarea</label>
                  </div>

                @endif
              <!-- the code below is used to set a divider -->
              <div class="divider" style="margin:10px 10px;"></div>
          @empty
              <!-- the code below is used to show nothing if there is nothing to show-->
            <span class='flow-text center-align'>Nothing to show</span>
          @endforelse
      <!-- the code below is used to submit the survey to the client-->
        {{ Form::submit('Submit Survey', array('class'=>'btn waves-effect waves-light')) }}
        {!! Form::close() !!}
    </div>
  </div>
@stop