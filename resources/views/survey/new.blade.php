<!-- the code below is used to bring the layout  -->
@extends('layout')
<!-- the code below is used to bring the content  -->
@section('content')
    <!-- the code below is used to create the class for a new survey  -->
  <div class="card">
      <div class="card-content">
          <!-- the code below is used to add the new survey  -->
      <span class="card-title"> Add Survey</span>
          <!-- the code below is used to create the survey ad boolean  -->
      <form method="POST" action="create" id="boolean">
          <!-- the code below is used to store the csrf token  -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <!-- the code below is used to create the row  -->
        <div class="row">    
          <div class="input-field col s12">
              <!-- the code below is used to set the title of the survey   -->
            <input name="title" id="title" type="text">
              <!-- the code below is a lable  -->
            <label for="title">Survey Title</label>
          </div>
            <!-- the code below is used to create the discription  -->
          <div class="input-field col s12">
            <textarea name="description" id="description" class="materialize-textarea"></textarea>
              <!-- the code below is a lable  -->
            <label for="description">Description</label>
          </div>
            <!-- the code below is used to submit  -->
          <div class="input-field col s12">
          <button class="btn waves-effect waves-light">Submit</button>
          </div>
        </div>
        </form>
    </div>
  </div>
@stop