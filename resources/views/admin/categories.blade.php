<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Categories</title>
</head>
<body>
<h1>Categories</h1>

<section>
    @if (isset ($categories))

        <ul>
            @foreach ($categories as $categories)
                <li>{{ $categories->title }}</li>
            @endforeach
        </ul>
    @else
        <p> no categories added yet </p>
    @endif
</section>

{!! Form::open(['']) !!}
<div class="row">
    {!! Form::submit('Add category', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}


</body>
</html>