<!-- the code below is used to bring the layout  -->
@extends('layout')
<!-- the code below is used to bring the content  -->
@section('content')
    <!-- the code below is used to update the questions -->
<form method="POST" action="/question/{{ $question->id }}/update">
  {{ method_field('PATCH') }}
  <!-- the code below is used to store the csrf token -->
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <!-- the code below is used to show the link for edit  -->
  <h2 class="flow-text">Edit Question Title</h2>
      <!-- the code below is used to create the row and input filed  -->
   <div class="row">
    <div class="input-field col s12">
        <!-- the code below is used for the title of the question being edited -->
      <input type="text" name="title" id="title" value="{{ $question->title }}">
        <!-- the code below is used to be a lable -->
      <label for="title">Question</label>
    </div>
       <!-- the code below is used to handle the update button  -->
    <div class="input-field col s12">
    <button class="btn waves-effect waves-light">Update</button>
    </div>
  </div>
</form>
@stop