<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    //the code below is the up to the db
    public function up()
    {
        //the code below is used to crate the table with all sections below
        Schema::create('Answer', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unique();
            $table->integer('question_id')->unique();
            $table->integer('survey_id')->unique();
            $table->string('answer')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    //the code below is the down to the db
    public function down()
    {
        //the code below is to drop the schema
        Schema::drop('Answer');
    }
}