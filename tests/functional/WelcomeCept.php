<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('perform actions and see result');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);
