<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    //the code below is used for the title discription and user id for a fillable protected section
    protected $fillable = ['title', 'description', 'user_id'];
    //the code below is used to dates and delete code
    protected $dates = ['deleted_at'];
    //the code below is used to add the survey to the code
    protected $table = 'survey';

    //the code below is used to handle many questions
    public function questions() {
        return $this->hasMany(Question::class);
    }
//the code is used to handle the belongs to user
    public function user() {
        return $this->belongsTo(User::class);
    }
//the code is used to handle has many ansewrs
    public function answers() {
        return $this->hasMany(Answer::class);
    }

}
