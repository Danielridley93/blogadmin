<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //the code below is do fill and add the the table the responce
    protected $fillable = ['answer'];
    protected $table = 'answer';

    //the code below is for the code to add belongs to the survey
    public function survey() {
      return $this->belongsTo(Survey::class);
    }
//the code below is to add the question belongs code
    public function question() {
      return $this->belongsTo(Question::class);
    }
}
