<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Survey;
use App\Answer;
use App\Http\Requests;


class AnswerController extends Controller
{
    //the code below is the public function for the auth and will construct it
  public function __construct()
  {
    $this->middleware('auth');
  }

  //the public function below is used to store the survey questions
  public function store(Request $request, Survey $survey) 
  {
    //the code below will remove the token
    $arr = $request->except('_token');
      //the code bellow is for the forreach loop that will then store the new ansewr as a value and key
    foreach ($arr as $key => $value) {
      $newAnswer = new Answer();
      if (! is_array( $value )) {
        $newValue = $value['answer'];
      } else {
        $newValue = json_encode($value['answer']);
      }
      //the code below is what stores the new ansewrs within the ansewrs db
      $newAnswer->answer = $newValue;
      $newAnswer->question_id = $key;
      $newAnswer->user_id = Auth::id();
      $newAnswer->survey_id = $survey->id;

      $newAnswer->save();

      $answerArray[] = $newAnswer;
    };
    //the code below is used to open the survey answers controller that is stored within the survey controller
    return redirect()->action('SurveyController@view_survey_answers', [$survey->id]);
  }
}
