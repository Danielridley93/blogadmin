<?php
 
namespace App\Http\Controllers;
use Auth;
use App\Survey;
use App\Answer;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
 
class SurveyController extends Controller
{
    //the code below is used to call the auth
    public function __construct()
    {
        $this->middleware('auth');
    }
//the code below is used to direct you to the home funtion
    public function home(Request $request)
    {
        $surveys = Survey::get();
        return view('home', compact('surveys'));
    }

    //the code below is used to show page to create new survey
    public function new_survey()
    {
        return view('survey.new');
    }
//the code below is used to create the auth and survey
    public function create(Request $request, Survey $survey)
    {
        $arr = $request->all();
        $arr['user_id'] = Auth::id();
        $surveyItem = $survey->create($arr);
        return Redirect::to("/survey/{$surveyItem->id}");
    }

    //the code below will retrieve detail page and add questions here
    public function detail_survey(Survey $survey)
    {
        $survey->load('questions.user');
        return view('survey.detail', compact('survey'));
    }

//the code beow is used to edit the survey
    public function edit(Survey $survey)
    {
        return view('survey.edit', compact('survey'));
    }

    //the code again is edit survey and update
    public function update(Request $request, Survey $survey)
    {
        $survey->update($request->only(['title', 'description']));
        return redirect()->action('SurveyController@detail_survey', [$survey->id]);
    }

    //the code below is used to view survey publicly and complete survey
    public function view_survey(Survey $survey)
    {
        $survey->option_name = unserialize($survey->option_name);
        return view('survey.view', compact('survey'));
    }

    //the code bellow is used view submitted answers from current logged in user
    public function view_survey_answers(Survey $survey)
    {
        $survey->load('user.questions.answers');
        return view('answer.view', compact('survey'));
    }
    //the code below is to delete the survey that has been created
    public function delete_survey(Survey $survey)
    {
        $survey->delete();
        return redirect('');
    }

}