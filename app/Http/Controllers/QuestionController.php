<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Question;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use App\Http\Requests;

class QuestionController extends Controller
{
    //the code below is rhe construct public fuction that is used to get auth
    public function __construct()
    {
        $this->middleware('auth');
    }
  //the code below is the public function that is used to store the user id and the request
    public function store(Request $request, Survey $survey) 
    {
      $arr = $request->all();
      $arr['user_id'] = Auth::id();

      $survey->questions()->create($arr);
      return back();
    }
//the code bellow is the public function that is used to edit the question
    public function edit(Question $question) 
    {
      return view('question.edit', compact('question'));
    }
//the code below is the public function used to update the question
    public function update(Request $request, Question $question) 
    {

      $question->update($request->all());
        //the code below is used to redirect you back to the detial of the survey
      return redirect()->action('SurveyController@detail_survey', [$question->survey_id]);
    }

}
