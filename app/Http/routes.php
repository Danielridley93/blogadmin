<?php

  /*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
  */

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('articles', 'OpenArticleController');
});

Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('articles', 'OpenCategoriesController');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::resource('/admin/articles', 'ArticleController' );
    Route::resource('/admin/users', 'UserController' );
});

Route::get('/', 'SurveyController@home')->name('home.page');
Route::auth();

Route::get('/home', 'HomeController@index');


Route::get('/myroute', function() {
    return view('home');
});


//the code below is used to view the survey
Route::get('/survey/view/{survey}', 'SurveyController@view_survey')->name('view.survey');
//the code below is used when making a new survey
Route::get('/survey/new', 'SurveyController@new_survey')->name('new.survey');
//the code below is used when making the survey go home
Route::get('/', 'SurveyController@home');
//the code below is used when making another new survey
Route::get('/survey/new', 'SurveyController@new_survey')->name('new.survey');
//this code below is used to get the detail of the survey
Route::get('/survey/{survey}', 'SurveyController@detail_survey')->name('detail.survey');
//the code below is used when viewing the survey
Route::get('/survey/view/{survey}', 'SurveyController@view_survey')->name('view.survey');
//the ode below is used to view the ansewrs from the survey
Route::get('/survey/answers/{survey}', 'SurveyController@view_survey_answers')->name('view.survey.answers');
//the code below is used to delete the survey
Route::get('/survey/{survey}/delete', 'SurveyController@delete_survey')->name('delete.survey');
//the code below is used to edit the survey
Route::get('/survey/{survey}/edit', 'SurveyController@edit')->name('edit.survey');
//the code below is used to update the survey
Route::patch('/survey/{survey}/update', 'SurveyController@update')->name('update.survey');
//the code below is used when the user has completed the survey
Route::post('/survey/view/{survey}/completed', 'AnswerController@store')->name('complete.survey');
//the code below is used when creating another survey
Route::post('/survey/create', 'SurveyController@create')->name('create.survey');
//the code below is ued when storing questions
Route::post('/survey/{survey}/questions', 'QuestionController@store')->name('store.question');
//the code below is used to edit the survey
Route::get('/question/{question}/edit', 'QuestionController@edit')->name('edit.question');
//the code below is used to update the survey
Route::patch('/question/{question}/update', 'QuestionController@update')->name('update.question');
//the code below is used to link the auth
Route::auth();
