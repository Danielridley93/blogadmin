<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //the code below is used to store the option name in and array
  protected $casts = [
      'option_name' => 'array',
  ];
    //the code below is used to fill the below sections
  protected $fillable = ['title', 'question_type', 'option_name', 'user_id'];
    //the code below is used to handle the belongs to survey
  public function survey() {
    return $this->belongsTo(Survey::class);
  }
//the code below is used to handle the belongs to user
  public function user() {
    return $this->belongsTo(User::class);
  }
//the code below is used to handle the has many ansewrs
  public function answers() {
    return $this->hasMany(Answer::class);
  }
    //the code below is used to handle the table question as protected
  protected $table = 'question';

}
